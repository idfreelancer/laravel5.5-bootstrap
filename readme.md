# Laravel 5.5 Boostrap



## About

This is starter kit for building Modular CMS/ Web Apps Based on Laravel 5.5. 


## How To Install

```
1. Clone this repo
2. Rename **env.example** to **.env** 
3. Run **$ composer install**
5. Run **$ bower install --save**
4. Run Install **php artisan qilara:install**
```


## Note
To publish asset from modules use : **php artisan vendor:publish --tag=modules --force**

