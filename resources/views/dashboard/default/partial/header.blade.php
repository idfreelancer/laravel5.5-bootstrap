<!-- Bootstrap core CSS -->
{!! Theme::css('css/bootstrap.min.css') !!}
{!! Theme::css('css/font-awesome.min.css') !!}
<link href="{!! asset('vendors/iCheck/skins/flat/green.css')!!}">
{!! Theme::css('css/animate.min.css') !!}

@yield('header_asset')

{!! Theme::css('css/custom.css') !!}
{!! Theme::css('css/qilara.css') !!}