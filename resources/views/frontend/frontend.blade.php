@extends('index')

@section('content')
	<div class="section type-2">
		<div class="container">
	    	<div class="section-headlines">
	        	<h4>Ini Sub Headernya</h4>
	            <span>Silahkan kakak-kakak mengisinya</span>
			</div>            
	    </div>
	</div>
@stop

@section('footer_asset')    
    {!! Theme::js('js/jquery.smooth-scroll.min.js') !!}
    {!! Theme::js('js/jquery.mixitup.min.js') !!}
    {!! Theme::js('js/jquery.easing.1.3.js') !!}
    {!! Theme::js('js/modernizr.js') !!}
    {!! Theme::js('js/jquery.fancybox.pack.js') !!}
    {!! Theme::js('js/custom.js') !!}

    <link rel="stylesheet" href="{!! asset('vendor/jquery-elastic-grid/css/elastic_grid.min.css')!!}">

    <script type="text/javascript" src="{!! asset('vendor/jquery-elastic-grid/js/jquery.hoverdir.js')!!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/jquery-elastic-grid/js/modernizr.custom.js')!!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/jquery-elastic-grid/js/classie.js')!!}"></script>
    <script type="text/javascript" src="{!! asset('vendor/jquery-elastic-grid/js/jquery.elastislide.js')!!}"></script>    
    <script type="text/javascript" src="{!! asset('vendor/jquery-elastic-grid/js/elastic_grid_demo.js')!!}"></script>
@stop