<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> {!! empty($title)? "" : $title ." |"!!}  {{ strip_tags(Config::get('qilara.site_name')) }}</title>
    <!-- Stylesheets -->
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- {!! Theme::css('css/bootstrap.min.css') !!} -->
    {!! Theme::css('https://fonts.googleapis.com/css?family=Roboto:400,400italic,700,700italic,500italic,500,300italic,300') !!}
    {!! Theme::css('icons/font-awesome/css/font-awesome.css') !!}
    {!! Theme::css('icons/rondo/style.css') !!}
    {!! Theme::css('css/jquery.fancybox.css') !!}
    {!! Theme::css('css/style.css') !!}
    {!! Theme::css('https://fonts.googleapis.com/css?family=Kaushan+Script') !!}
</head>
<body >
    <header class="header" id="jump">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
                            class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <!--<img src="img/logo.png" alt="" />-->
                        <span style="font-family: 'Kaushan Script', cursive;color: #84c225;font-size: 28px">Site Name</span>
                    </a>
                </div>
                <div class="collapse navbar-collapse hidden-xs navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="{!! route('frontend.index') !!}">Home</a></li>
                        <li><a href="#jump6">Contact Us</a></li>
                        <li><a href="{{ route('register') }}" class="register_link">Register</a></li>
                    </ul>
                    
                    @if (\Auth::guard('customer')->check())
                       <div class="btn-group" style="margin-top: 5px">
                            <button type="button" class="btn btn-success">
                                {{ \Auth::guard('customer')->user()->name}}
                            </button>
                            <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('reservation.list')}}">Reservasi</a></li>
                                <li><a href="{{ route('frontend.logout')}}">Logout</a></li>
                            </ul>
                        </div>
       
                    @else            
                        {!! Form::open(array('url' => route('customer.login'), 'class' => 'navbar-form navbar-right', 'method' => 'post', 'id' => 'signin', 'role' => 'form')) !!}                   

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input id="email" type="email" class="form-control col-md-2" name="email" value="" placeholder="Email Address">                                        
                        </div>

                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input id="password" type="password" class="form-control col-md-2" name="password" value="" placeholder="Password">                                        
                        </div>

                        <button type="submit" class="btn btn-primary">Login</button>
                        </form>
                    @endif
                   </div>
                <!-- /.navbar-collapse -->
                <!--
                <div class="collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#jump0" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            Home</a></li>                        
                        <li><a href="#jump6" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            Contact</a></li>                        
                    </ul>
                </div>
                -->
                <!-- /.navbar-responsive-collapse -->
            </div>
        </nav>
    </header>
        <div style="margin-top: 75px;">
        @if (Session::has('message'))
            <div class="container">            
                <div class="col-md-offset-3 col-md-6">
                    <div class="alert alert-danger" role="alert">
                        <p>{{ Session::get('message') }}</p>
                    </div>
                </div>
            </div>
        @endif
            <div class="section type-2">
                <div class="container">
                    <div class="section-headlines">
                        @yield('content')
                    </div>
                </div>  
            </div>  
        </div>   
    
    <div class="section type-1 section-contact">
        <div class="container">
            <div class="section-headlines">
                <h4>Contact Us</h2>                    
            </div>
            <form>
            <div class="row">
                <div class="col-lg-4">
                    <address>
                        <div class="address-row">
                            <div class="address-sign">
                                <i class="icon-map-marker"></i>
                            </div>
                            <div style="address-info">
                                <b>Company Name</b>,<br>
                                Companny Address 1<br>
                                Companny Address 2<br>
                                City
                            </div>
                        </div>
                    </address>
                    <div class="visible-xs visible-sm">
                        <br class="gap-30" />
                        <hr class="gap-divider" />
                        <br class="gap-30" />
                    </div>
                </div>
                <div class="col-lg-7 col-lg-offset-1">
                    <form role="form" method="post" action="#" id="contactform">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Your Name *"><br
                                    class="gap-15" />
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" id="email" name="email" placeholder="Your Email *"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="message" name="message" rows="8"></textarea>
                    </div>
                    <button id="button-send" class="btn btn-block btn-success">
                        Send Message Now
                    </button>
                    <div id="success">
                        Your message has been successfully!</div>
                    <div id="error">
                        Unable to send your message, please try later.</div>
                    </form>
                </div>
            </div>
            </form>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    Andri Fachrur Rozie &copy; 2017 </a>
                </div>
            </div>
        </div>
    </footer>

    <!--Scripts-->
    {!! Theme::js('js/jquery-1.10.2.min.js') !!}
    {!! Theme::js('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js') !!}
    <script type="text/javascript">
        $(document).ready(function(){
            $('.register_link').click(function(){
                window.location($(this).attr("href"));
            });
        });
    </script>    
    @yield('footer_asset')
</body>
</html>
