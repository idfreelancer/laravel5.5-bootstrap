@extends('index')

@section('content')
	<div class="panel panel-default col-md-offset-2 col-lg-8 col-md-8">
		<div class="panel-body">
		<div class="row">	            
			<div class="col-lg-12 col-md-12">
			{!! Form::open(array('url' => route('customer.registering'), 'class' => 'form-horizontal', 'method' => 'post', )) !!}            		
	    	<div class="form-body">
	            <div class="form-group">
	                <label for="inputName" class="col-md-3 control-label col-xs-3">
	                    {{ trans('common.name') }}
	                </label>
	                <div class="col-md-9 col-xs-9">
	                    {!! Form::text('name',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
	                </div>
	            </div>
	            <div class="form-group">
	                <label for="inputName" class="col-md-3 control-label col-xs-3">
	                    {{ trans('common.email') }}
	                </label>
	                <div class="col-md-9 col-xs-9">
	                    {!! Form::text('email',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
	                </div>
	            </div>
	            <div class="form-group">
	                <label for="inputName" class="col-md-3 control-label col-xs-3">
	                    {{ trans('common.password') }}
	                </label>
	                <div class="col-md-9 col-xs-9">
	                    <input id="inputName" class="form-control" data-validate-length-range="4,20" required="required" type="password" name="password">
	                </div>
	            </div>
	            <div class="form-group">
	                <label for="inputName" class="col-md-3 control-label col-xs-3">
	                    {{ trans('common.repeat_password') }}
	                </label>
	                <div class="col-md-9 col-xs-9">
	                    <input id="inputName" class="form-control" data-validate-length-range="4,20" required="required" type="password" name="password_confirmation">
	                </div>
	            </div>
	            <div class="form-group">
	                <label for="inputName" class="col-md-3 control-label col-xs-3">
	                    {{ trans('common.address') }}
	                </label>
	                <div class="col-md-9 col-xs-9">
	                    {!! Form::text('address',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
	                </div>
	            </div>
	            <div class="form-group">
	                <label for="inputName" class="col-md-3 control-label col-xs-3">
	                    {{ trans('common.handphone') }}
	                </label>
	                <div class="col-md-9 col-xs-9">
	                    {!! Form::text('phone',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
	                </div>
	            </div>
	            <div class="form-group">
	                <label for="inputName" class="col-md-3 control-label col-xs-3">
	                    {{ trans('common.institution') }}
	                </label>
	                <div class="col-md-9 col-xs-9">
	                    {!! Form::text('institution',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
	                </div>
	            </div>
	            <div class="form-group">
	                <label for="inputName" class="col-md-3 control-label col-xs-3">
	                    {{ trans('common.job_title') }}
	                </label>
	                <div class="col-md-9 col-xs-9">
	                    {!! Form::text('job_title',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
	                </div>
	            </div>
	            <div class="form-actions pal">
	                <div class="form-group mbn">
	                    <div class="col-md-3 col-xs-3 right" style="text-align: right;">
	                        <input type="hidden" name="items">
	                        <button type="submit" class="btn btn-primary">Submit</button>
	                    </div>
	                </div>
	            </div>
	        </div>
	        </form>
	    </div>
	</div>
	</div>		  		
@stop