<!DOCTYPE HTML>
<html>
	<head>
		<title>404 Error</title>		
		<link href="{!! asset('css/404.css') !!}" rel="stylesheet" type="text/css"  media="all" />
	</head>
	<body>		
		<div class="wrap">		
				<div class="header">
					<div class="logo">
						<h1><a href="#">Ohh</a></h1>
					</div>
				</div>			
			<div class="content">
				<img src="{!! asset('images/404/error-img.png') !!}" title="error" />
				<p><span><label>O</label>hh.....</span>You Requested the page that is no longer There.</p>
				<a href="javascript: history.go(-1)">Back</a>
				<div class="copy-right">
					<p>&#169 All rights Reserved</a></p>
				</div>
   			</div>
		</div>
	</body>
</html>

