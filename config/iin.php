<?php

return [
    'site_name' => env('QILARA_SITE_NAME', 'Sistem Informasi Manajemen Laboratorium'),
    'institution_short' => env('QILARA_SHORT_NAME', 'SIML'),
    'institution' => 'Pusat Penelitian Informatika'
];