<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

// Route::get('/', function(){
//     return redirect('dashboard');
// });

Route::get('/', ['uses' => 'FrontpageController@index', 'as' => 'frontend.index']);
Route::get('register', ['uses' => 'FrontpageController@register', 'as' => 'register']);
Route::post('register', ['uses' => 'FrontpageController@customer_store', 'as' => 'customer.registering']);
Route::post('customer_login', ['uses' => 'FrontpageController@customer_login', 'as' => 'customer.login']);
Route::get('welcome', ['uses' => 'FrontpageController@welcome', 'as' => 'welcome']);
Route::get('logout', ['uses' => 'FrontpageController@logout', 'as' => 'frontend.logout']);
Route::post('reservation/{id}', ['uses' => 'FrontpageController@reservation_store', 'as' => 'reservation.store']);

Route::get('reservation/{id}', ['uses' => 'FrontpageController@reservation', 'as' => 'reservation']);
Route::get('reservation_list', ['uses' => 'FrontpageController@reservation_list', 'as' => 'reservation.list']);

Route::get('dashboard/login', ['uses' => 'DashboardController@login','middleware' => 'guest', 'as' => 'dashboard.login']);
Route::post('dashboard/login', ['uses' => 'DashboardController@postLogin', 'as' => 'dashboard.login_process']);

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function() {
    Theme::set('dashboard_default');

    Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'dashboard']);
    Route::get('logout', 'DashboardController@logout');

    Route::get('logs', ['uses' => 'DashboardController@Logs', 'as' => 'dashboard.logs']);
});