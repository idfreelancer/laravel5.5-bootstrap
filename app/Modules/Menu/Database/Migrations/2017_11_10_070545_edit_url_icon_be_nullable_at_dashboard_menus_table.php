<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditUrlIconBeNullableAtDashboardMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dashboard_menus', function (Blueprint $table) {
            $table->string('url')->nullable()->change();
            $table->string('icon')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dashboard_menus', function (Blueprint $table) {
            $table->string('url')->nullable(false)->change();
            $table->string('icon')->nullable(false)->change();
        });
    }
}
