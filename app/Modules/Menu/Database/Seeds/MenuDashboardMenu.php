<?php

namespace App\Modules\Menu\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Modules\Menu\Models\DashboardMenu;

class MenuDashboardMenu extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu_parent = [
        	'name'         => 'Menu',
        	'icon'         => 'fa fa-sitemap', 
            'permission'   =>  'dashboard_menu-index',
        	'parent_id'    => 0, 
        	'order'	       => 1
        ];

        $menu_child = [
	        	'name'    => 'Dashboard Menu', 
	        	'route'   => 'dashboard_menu.index',
                'permission'   =>  'dashboard_menu-index',
	        	'icon'    => 'fa fa-sitemap', 
	        	'order'	  => 1
        ];

        $menu = DashboardMenu::create($menu_parent);

        $child = new DashboardMenu;
        $child->fill($menu_child);
        $child->parent_id = $menu->id;
        $child->save();
    }
}
