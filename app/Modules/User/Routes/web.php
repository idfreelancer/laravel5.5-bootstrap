<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function() {
	Route::group(['prefix' => 'users'], function () {
		Route::get('/', ['uses' => 'UserController@index', 'as' => 'users.index', 'middleware' => ['permission:users-index']]);
		Route::get('create', ['uses' => 'UserController@create', 'as' => 'users.create', 'middleware' => ['permission:users-create']]);
		Route::post('store', ['uses' => 'UserController@store', 'as' => 'users.store', 'middleware' => ['permission:users-store']]);
		Route::get('{id}/edit', ['uses' => 'UserController@edit', 'as' => 'users.edit', 'middleware' => ['permission:users-edit']]);
		Route::match(['put', 'patch'],'{id}', ['uses' => 'UserController@update', 'as' => 'users.update', 'middleware' => ['permission:users-update']]);
		Route::delete('{id}', ['uses' => 'UserController@destroy', 'as' => 'users.destroy', 'middleware' => ['permission:users-destroy']]);
		Route::post('store_picture', ['uses' => 'UserController@storePicture', 'as' => 'users.store_picture']);
	});

	Route::group(['prefix' => 'roles'], function()
    {
    	Route::group(['prefix' => 'user_roles'], function()
	    {
			Route::get('/', ['uses' => 'RoleController@role_index', 'as' => 'roles.index', 'middleware' => ['permission:roles-index']]);
			Route::get('create', ['uses' => 'RoleController@role_create', 'as' => 'roles.create', 'middleware' => ['permission:roles-create']]);
			Route::post('store', ['uses' => 'RoleController@role_store', 'as' => 'roles.store', 'middleware' => ['permission:roles-store']]);
			Route::get('{id}/edit', ['uses' => 'RoleController@role_edit', 'as' => 'roles.edit', 'middleware' => ['permission:roles-edit']]);
			Route::match(['put', 'patch'],'{id}', ['uses' => 'RoleController@role_update', 'as' => 'roles.update', 'middleware' => ['permission:roles-update']]);
			Route::delete('{id}', ['uses' => 'RoleController@permission_destroy', 'as' => 'roles.destroy', 'middleware' => ['permission:roles-destroy']]);
	    });

	    Route::group(['prefix' => 'permissions'], function()
	    {
			Route::get('/', ['uses' => 'RoleController@permission_index', 'as' => 'permissions.index', 'middleware' => ['permission:permissions-index']]);
			Route::get('create', ['uses' => 'RoleController@permission_create', 'as' => 'permissions.create', 'middleware' => ['permission:permissions-create']]);
			Route::post('store', ['uses' => 'RoleController@permission_store', 'as' => 'permissions.store', 'middleware' => ['permission:permissions-store']]);
			Route::get('{id}/edit', ['uses' => 'RoleController@permission_edit', 'as' => 'permissions.edit', 'middleware' => ['permission:permissions-edit']]);
			Route::match(['put', 'patch'],'{id}', ['uses' => 'RoleController@permission_update', 'as' => 'permissions.update', 'middleware' => ['permission:permissions-update']]);
			Route::delete('{id}', ['uses' => 'RoleController@permission_destroy', 'as' => 'permissions.destroy', 'middleware' => ['permission:permissions-destroy']]);
	    });
    });
});