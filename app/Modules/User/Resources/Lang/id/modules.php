<?php

return [
    'roles_management' => 'Roles Management',
    'add_roles' => 'Add Roles',
    'add_user' => 'Tambah User',
    'role' => 'Role',
    'users' => 'User',
    'users_list' => 'Daftar User',
    'users_deleted' => 'User telah Dihapus',
    'user_created' => 'User telah Ditambahkan',
    'user_updated' => 'User telah Diupdate',
    'role_permission' => 'Hak User',
    'role' => 'Grup User',
    'permission' => 'Wewenang',
    'handphone' => 'Handphone',
    'institution' => 'Institusi',
    'job_title' => 'Posisi/ Jabatan',
    'birth_place' => 'Tempat Lahir',
    'birth_date' => 'Tanggal Lahir',
    'address' => 'Alamat',
    
 ];