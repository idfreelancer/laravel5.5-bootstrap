@extends('../index')

@section('content')
    @if (Session::has('message'))
        <div class="row">
            <div class="alert alert-success" role="alert">
                <p>{{ Session::get('message') }}</p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="row">
            <div class="alert alert-error" role="alert">
                <p>{{$errors->first()}}</p>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ $title }}
                    </h2>
                    <div class="form-group" style="float: right;">
                        <a href="{{ route('roles.create') }}" class="btn btn-success">
                            <i class="fa fa-plus-circle"></i> {{ trans('common.add') }} {{ $title }}
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div>
                        <table class="table table-striped responsive-utilities jambo_table" id="user_list" style="width: 100%">
                            <thead>
                            <tr>
                                <th>{{ trans('common.name') }}</th>
                                <th width="20%">{{ trans('common.description') }}</th>
                                <th width="15%">{{ trans('common.last_update') }}</th>
                                <th width="50px"></th>
                            </tr>
                            </thead>
                            <tbody>                            
                            @if($data->isEmpty())
                                <tr><td colspan="4"><p class="center_bold">Tidak Ada Data</p></td></tr>
                            @else
                                @foreach($data as $row)
                                    <tr>
                                        <td>
                                            <a href="{{route('roles.edit', $row->id)}}" class="label label-info">                                                
                                                {!! empty($row->display_name)? $row->name : $row->display_name !!}
                                            </a>
                                        </td>                                        
                                        <td>{{ $row->description}}</td>                                        
                                        <td>{{ localeDate($row->updated_at) }}</td>
                                        <td class="center">
                                            <a href="{{route('permissions.destroy', $row->id)}}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="{{ trans('common.are_you_sure') }}"><span class="fa fa-trash-o"></span></a>                                        
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer_js')    
    {!! Theme::js('js/datatables.net/js/jquery.dataTables.min.js')!!}    
    {!! Theme::js('js/confirm.js')!!}
    
    <script type="text/javascript">
        $(document).ready(function()
        {
            var userTable = $('#user_list').DataTable({
                sPaginationType: "full_numbers",
                /*columnDefs: [
                    { width: "250px", targets : 4}
                ],*/
                "bFilter": true,
            });

            $('#msds-select').change( function() {
                userTable
                        .columns(2)
                        .search(this.value, true, false)
                        .draw();
            });
        })
    </script>
@stop