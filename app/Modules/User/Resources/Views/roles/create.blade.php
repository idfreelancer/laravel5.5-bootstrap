@extends('../index')

@section('content')
    @if (Session::has('message'))
        <div class="row">
            <div class="alert alert-success" role="alert">
                <p>{{ Session::get('message') }}</p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="row">
            <div class="alert alert-error" role="alert">
                <p>{{$errors->first()}}</p>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ $title }}
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-lg-12 col-md-12">
                        @if (Route::getCurrentRoute()->getName() == 'roles.edit')
                            {!! Form::model($data, array('url' => route('roles.update', Request::segment(4)), 'class' => 'form-horizontal', 'method' => 'put')) !!}
                        @else
                            {!! Form::open(array('url' => route('roles.store'), 'class' => 'form-horizontal', 'method' => 'post')) !!}
                        @endif
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputName" class="col-md-1 control-label col-xs-1">
                                    {{ trans('common.name') }}
                                </label>
                                <div class="col-md-11 col-xs-11">
                                    {!! Form::text('display_name',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-1 control-label col-xs-1">
                                    {{ trans('common.description') }}
                                </label>
                                <div class="col-md-11 col-xs-11">
                                    {!! Form::textarea('description',null, array('id' => 'inputName', 'class' => 'form-control description', 'required' => true)) !!}
                                </div>
                            </div>
                            
                            
                            <table class="table table-striped responsive-utilities jambo_table" id="user_list" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th style="width: 20px"></th>
                                        <th>Permission</th>
                                        <th>Description</th>
                                        <th style="width: 120px"></th>
                                    </tr>
                                </thead>
                                @foreach ($permission as $prefix => $perm_list)
                                    @if (is_string($prefix))
                                        <tr>
                                            <th colspan="4">{{ ucfirst($prefix) }}</th>
                                        </tr>
                                        @foreach ($perm_list as $key => $val)                                            
                                            <tr>
                                                <td></td>
                                                <td>{!! empty($val['display_name'])? $val['name'] : $val['display_name'] !!}</td>                                                
                                                <td>{{ $val['description'] }}</td>
                                                <td>
                                                    <input type="checkbox" class="js-switch" {{ is_checked($val['id'], $cur_permission) }} 
                                                        name="permission[{{$val['name']}}]" value="{{ $val['id'] }}" />
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else                                        
                                        @foreach ($perm_list as $key => $val)
                                            <tr>
                                                <th colspan="4">{{ ucfirst($val['name']) }}</th>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>{!! empty($val['display_name'])? $val['name'] : $val['display_name'] !!}</td>
                                                <td>{{ $val['description'] }}</td>
                                                <td>
                                                    <input type="checkbox" class="js-switch" {{ is_checked($val['id'], $cur_permission) }} 
                                                        name="permission[{{$val['name']}}]" value="{{ $val['id'] }}" />
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif                                    
                                @endforeach
                            </table>
                            <div class="form-actions pal">
                                <div class="form-group mbn">
                                    <div class="col-md-2 col-xs-2 right">
                                        <input type="hidden" name="items">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>  
@stop

@section('header_asset')
    {!! Theme::css('css/switchery.min.css')!!}    
@stop

@section('footer_asset')    
    {!! Theme::js('js/switchery.min.js')!!}
    {!! Theme::js('js/qilara.js')!!}
@stop