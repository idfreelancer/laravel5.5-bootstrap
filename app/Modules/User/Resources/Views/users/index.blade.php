@extends('../index')

@section('content')
    @if (Session::has('message'))
        <div class="row">
            <div class="alert alert-success" role="alert">
                <p>{{ Session::get('message') }}</p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="row">
            <div class="alert alert-error" role="alert">
                <p>{{$errors->first()}}</p>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ $title }}
                    </h2>
                    <div class="form-group" style="float: right;">
                        <a href="{{ route('users.create') }}" class="btn btn-success">
                            <i class="fa fa-plus-circle"></i> {{ trans('common.add_user') }}
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div>
                        <table class="table table-striped responsive-utilities jambo_table" id="table_wdelete" style="width: 100%">
                            <thead>
                            <tr>
                                <th>{{ trans('common.name') }}</th>
                                <th width="20%">{{ trans('common.username') }}</th>
                                <th width="15%">{{ trans('common.email') }}</th>                                
                                <th width="15%">{{ trans('user::modules.role') }}</th>
                                <th width="200px">{{ trans('common.register_date') }}</th>
                                <th width="50px"></th>
                            </tr>
                            </thead>
                            <tbody>                            
                            @foreach($data as $row)
                                <tr>
                                    <td>
                                        <a href="{{route('users.edit', $row->id)}}" class="label label-info">
                                            {{ $row->name }}
                                        </a>
                                    </td>
                                    <td>{{ $row->username}}</td>
                                    <td>{{ $row->email}}</td>
                                    <td>                                        
                                        @foreach ($row->roles as $roles)
                                            <span class="label label-warning">
                                                {{ $roles->display_name }}
                                            </span>
                                        @endforeach
                                    </td>
                                    <td>{{ localeDate($row->created_at) }}</td>
                                    <td class="center">
                                        <a href="{{route('users.destroy', $row->id)}}" data-method="delete" data-token="{{csrf_token()}}" data-confirm="{{ trans('common.are_you_sure') }}"><span class="fa fa-trash-o"></span></a>                                        
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('footer_asset')
    {!! Theme::js('js/datatables.net/js/jquery.dataTables.min.js')!!}    
    {!! Theme::js('js/confirm.js')!!}    
@stop