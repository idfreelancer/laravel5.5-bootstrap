@extends('../index')

@section('content')
    @if (Session::has('message'))
        <div class="row">
            <div class="alert alert-success" role="alert">
                <p>{{ Session::get('message') }}</p>
            </div>
        </div>
    @endif

    @if ($errors->any())
        <div class="row">
            <div class="alert alert-error" role="alert">
                <p>{{$errors->first()}}</p>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        {{ trans('common.add_user') }}
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="col-lg-12 col-md-12">
                        @if (Route::getCurrentRoute()->getName() == 'users.edit')
                            {!! Form::model($data, array('url' => route('users.update', Request::segment(3)), 'class' => 'form-horizontal', 'method' => 'put', 'id' => 'add_procurement', 'files' => true)) !!}
                        @else
                            {!! Form::open(array('url' => route('users.store'), 'class' => 'form-horizontal', 'method' => 'post', 'id' => 'add_procurement', 'files' => true)) !!}
                        @endif
                        <div class="form-body">
                            <div class="form-group">
                                <label for="inputName" class="col-md-3 control-label col-xs-2">
                                    {{ trans('common.name') }}
                                </label>
                                <div class="col-md-9 col-xs-9">
                                    {!! Form::text('name',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-3 control-label col-xs-2">
                                    {{ trans('common.username') }}
                                </label>
                                <div class="col-md-9 col-xs-9">
                                    {!! Form::text('username',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
                                </div>
                            </div>
                             <div class="form-group">
                                <label for="inputName" class="col-md-3 control-label col-xs-2">
                                    {{ trans('common.email') }}
                                </label>
                                <div class="col-md-9 col-xs-9">
                                    {!! Form::text('email',null, array('id' => 'inputName', 'class' => 'form-control', 'required' => true)) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-3 control-label col-xs-2">
                                    {{ trans('user::modules.role') }}
                                </label>
                                <div class="col-md-9 col-xs-9">                                    
                                    {!! Form::select('role', $role_list, isset ($data->roles[0]->id)?$data->roles[0]->id:null, array('id' => 'inputName', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-3 control-label col-xs-2">
                                    {{ trans('common.password') }}
                                </label>
                                <div class="col-md-9 col-xs-9">
                                    {!! Form::password('password',array('id' => 'inputName', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputName" class="col-md-3 control-label col-xs-2">
                                    {{ trans('common.repeat_password') }}
                                </label>
                                <div class="col-md-9 col-xs-9">
                                    {!! Form::password('password_confirmation', array('id' => 'inputName', 'class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-actions pal">
                                <div class="form-group mbn">
                                    <div class="col-md-2 col-xs-2 right">
                                        <input type="hidden" name="items">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>  
@stop

@section('footer_js')
    {!! Theme::css('css/bootstrap-datepicker.min.css')!!}
    {!! Theme::css('css/jquery.bootstrap-touchspin.css')!!}
    {!! Theme::js('js/bootstrap-datepicker.min.js')!!}
    {!! Theme::js('js/jquery.bootstrap-touchspin.min.js')!!}
    {!! Theme::js('js/qilara.js')!!}
@stop